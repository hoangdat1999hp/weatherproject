package gst.trainingcourse.weatherapp

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import android.widget.RemoteViews
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

import java.security.AccessController.getContext
import kotlin.math.log
import kotlin.math.round


class MyService : Service() {
    private val mCompositeDisposable = CompositeDisposable()
    private var celmin1 = ""
    private var celmax2 = ""
    private var wether1 = ""
    private var icon = ""
    private val TAG :String = "TAG"
    private var notificationManager: NotificationManagerCompat? = null
    var cityname : String = ""

    override fun onBind(intent: Intent): IBinder? {

        Log.d(TAG, "onBind: ")
        return null
    }
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate() {
        super.onCreate()
        Log.d(TAG, "onCreate: ")

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("TAG", "onStartCommand: 123")
        cityname = intent!!.getStringExtra("cityname").toString()
        mCompositeDisposable.add(
            RetrofitClient.getAPI().getData(cityname).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    celmin1  = round(it.list.get(0).main.temp_min).toInt().toString()
                    celmax2  = round(it.list.get(0).main.temp_max).toInt().toString()
                    wether1 = it.list.get(0).weather.get(0).main
                    icon = it.list.get(0).weather.get(0).icon
                    if (celmin1.isNullOrEmpty()  ){
                        Log.d("TAG", "Bằng null ko show")
                    }
                    else {
                        showNotify(celmin1,celmax2,wether1,icon)
                        Log.d("TAG", "onStartCommand: shownotify " + celmin1 + celmax2 + wether1 +cityname)

                    }
                }, {
                    Log.d("==", "loadData: ${it.message}")
                })
        )

        return START_NOT_STICKY

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAG", "onDestroy: ")
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun showNotify(celmin1: String, celmax2: String, wether1 : String, icon : String){
        val CHANNEL_ID = "id_notify"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                CHANNEL_ID, "Example Channel",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            val manager = getSystemService(NotificationManager::class.java)
            manager.createNotificationChannel(channel)
        }

        val intent = Intent(this, MainActivity2::class.java)
        val collapsedView = RemoteViews(packageName, R.layout.notify_collapsed)
        val expandedView = RemoteViews(packageName, R.layout.notify_expanded)


        collapsedView.setTextViewText(R.id.text_nhiet_do_notify_collapsed, celmin1 +"°C" +" - " + celmax2 +"°C")
        collapsedView.setTextViewText(R.id.text_hientuong_notify_collapsed,wether1)
        collapsedView.setTextViewText(R.id.text_thanhpho_notify_collapsed,cityname)

        expandedView.setTextViewText(R.id.text_notify_expaned_cityname,cityname)
        expandedView.setTextViewText(R.id.text_notify_expaned_nhietdo, celmin1 +"°C" +" - " + celmax2 +"°C")
        expandedView.setTextViewText(R.id.text_notify_expaned_thoitiet, wether1)

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
            .setSmallIcon(R.drawable.cloud)
            .setStyle(NotificationCompat.DecoratedCustomViewStyle())
            .setContentIntent(PendingIntent.getActivity(this, 0, intent, 0))
            .setContentTitle(cityname)
//            .setContentText(celmin1 +"°C" +" - " + celmax2 +"°C")
            .setCustomContentView(collapsedView)
            .setCustomBigContentView(expandedView)
            .build()

        Picasso.with(getApplicationContext()).load("${RetrofitClient.URL_THUMBNAIL}${
            icon
        }" + ".png")
            .into(expandedView, R.id.img_notify_expaneded_weather, 1, notification)
        Picasso.with(getApplicationContext()).load("${RetrofitClient.URL_THUMBNAIL}${
            icon
        }" + ".png")
            .into(collapsedView, R.id.img_notify_logo_wether, 1, notification);
        notificationManager = NotificationManagerCompat.from(this);

        notificationManager!!.notify(1, notification)

        startForeground(1, notification)
//        stopForeground(false)

    }




}