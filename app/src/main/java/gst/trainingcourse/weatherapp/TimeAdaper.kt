package gst.trainingcourse.weatherapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import gst.trainingcourse.weatherapp.models.Custom_gio


class TimeAdaper(var listtime: MutableList<Custom_gio>, var context: Context) : RecyclerView.Adapter<TimeAdaper.ViewHolder>() {
    class ViewHolder(itemview: View): RecyclerView.ViewHolder(itemview) {
        var mua: TextView
        var gio: TextView
        var nhietdo: TextView
        var image:ImageView

        init {
            mua = itemView.findViewById(R.id.mua)
            gio = itemView.findViewById(R.id.gio)
            nhietdo = itemView.findViewById(R.id.nhietdo)
            image = itemview.findViewById(R.id.luongmua)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.item_gio, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        holder.mua.text = listtime.get(position).mua
        holder.gio.text = listtime.get(position).gio
        holder.nhietdo.text = listtime.get(position).temp
        Picasso.with(context).load(
            "${RetrofitClient.URL_THUMBNAIL}${
                listtime.get(position).icon
            }" + ".png"
        )
            .into(holder.image)
    }

    override fun getItemCount(): Int {
       return listtime.size
    }


}