package gst.trainingcourse.weatherapp




import gst.trainingcourse.weatherapp.models.We
import gst.trainingcourse.weatherapp.models2.Wex
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface IDocAPI {
    @GET("forecast?appid=033983e61de6d1ebcda9f01246820e8c&units=metric")
    fun  getData(@Query("q") cityNam:String):Observable<We>
    @GET("daily?appid=be8d3e323de722ff78208a7dbb2dcd6f&units=metric")
    fun  getData2(@Query("q") cityNam:String):Observable<Wex>
}