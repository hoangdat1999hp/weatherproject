package gst.trainingcourse.weatherapp

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import gst.trainingcourse.weatherapp.models.Custom_gio
import gst.trainingcourse.weatherapp.roomdb.Cityroom
import gst.trainingcourse.weatherapp.roomdb.Roomdb
import kotlinx.android.synthetic.main.item_history.view.*
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class HistoryAdapter(var listtime: MutableList<Cityroom>, var context: Context) : RecyclerView.Adapter<HistoryAdapter.ViewHolder>() {
    lateinit var itemClickOn: ItemClickOn
    private lateinit var roomDB: Roomdb
    class ViewHolder(itemview: View): RecyclerView.ViewHolder(itemview) {
        var mua: TextView
        var image: ImageView
        var temp: TextView
        var time: TextView
        var date: TextView

        init {
            mua = itemView.findViewById(R.id.cityname)
            image = itemview.findViewById(R.id.image)
            temp = itemview.findViewById(R.id.nd)
            time = itemview.findViewById(R.id.time)
            date = itemview.findViewById(R.id.date)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.item_history, parent, false)
        roomDB = Roomdb.getAppdatabase(parent.context)!!
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder:ViewHolder, position: Int) {
        holder.mua.text = listtime.get(position).name
        holder.itemView.rowHistory.setOnClickListener {
            itemClickOn.onClick(position)
        }
        holder.itemView.setOnLongClickListener (View.OnLongClickListener() {
            val builder = AlertDialog.Builder(it.context)
            builder.setTitle("Delete ${listtime[position].name} at ${listtime[position].time}?")
            builder.setMessage("Are you sure you want to delete?")
            builder.setPositiveButton("Yes", object: DialogInterface.OnClickListener{
                override fun onClick(dialog: DialogInterface?, which: Int) {
                    CoroutineScope(Dispatchers.Main).launch {
                        roomDB.cityroomDAO()?.delete(listtime[position])
                        Toast.makeText(
                            context,
                            "Successfully removed: ${listtime[position].name} at ${listtime[position].time}",
                            Toast.LENGTH_SHORT).show()
                        listtime.remove(listtime[position])
                        notifyDataSetChanged()
                    }
                }
            }).setNegativeButton("No", object : DialogInterface.OnClickListener {
                override fun onClick(dialog: DialogInterface?, which: Int) {

                }
            })

            builder.create().show()
            true

        })

        Picasso.with(context).load(
            "${RetrofitClient.URL_THUMBNAIL}${
                listtime.get(position).icon
            }" + ".png"
        )
            .into(holder.image)
        holder.temp.text = listtime.get(position).nhietdo
        holder.time.text = listtime.get(position).time
        holder.date.text = listtime.get(position).date
    }

    override fun getItemCount(): Int {
        return listtime.size
    }

}