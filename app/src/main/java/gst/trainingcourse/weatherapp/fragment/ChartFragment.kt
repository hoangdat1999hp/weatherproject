package gst.trainingcourse.weatherapp.fragment

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.charts.CombinedChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.*
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.highlight.Highlight
import com.github.mikephil.charting.listener.OnChartValueSelectedListener
import gst.trainingcourse.weatherapp.R
import gst.trainingcourse.weatherapp.RetrofitClient
import gst.trainingcourse.weatherapp.models.dailyWeather
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.round

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [ChartFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ChartFragment(var cityname: String) : Fragment(), OnChartValueSelectedListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private var mChart: CombinedChart? = null
//    private var mBChart: BarChart? = null
    private val mCompositeDisposable = CompositeDisposable()
    private var arrayList: ArrayList<dailyWeather> = arrayListOf()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view: View = inflater!!.inflate(R.layout.fragment_chart, container, false)
        mChart = view.findViewById(R.id.linechart)
//        mChart = view.findViewById<View>(R.id.linechart) as CombinedChart
        loadLineChart(requireContext(), cityname)
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment ChartFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            ChartFragment("").apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
    private fun loadLineChart(context: Context, cityname: String) {
        mCompositeDisposable.add(
            RetrofitClient.getAPI2().getData2(cityname).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    for (i in 0..6) {
                        val humidity = it.list.get(i).humidity.toString()
                        var thu = it.list.get(i).dt
                        val date = Date(thu * 1000L)
                        val simpleDateFormat = SimpleDateFormat("EEEE")
                        val day = simpleDateFormat.format(date)
                        val main = round(it.list.get(i).temp.day).toInt().toString()
                        val temp = it.list.get(i).temp.day
                        Log.d("TAG", temp.toString())
                        arrayList.add(dailyWeather(day, temp, humidity))
                    }
                    mChart?.getDescription()?.setEnabled(false)
                    mChart?.setBackgroundColor(Color.TRANSPARENT)
                    mChart?.setDrawGridBackground(false)

                    mChart?.setDrawBarShadow(false);
                    mChart?.setHighlightFullBarEnabled(false);
                    mChart?.setOnChartValueSelectedListener(this)
                    val l: Legend = mChart!!.getLegend()
                    l.setWordWrapEnabled(true)
                    l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM)
                    l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER)
                    l.setOrientation(Legend.LegendOrientation.HORIZONTAL)
                    l.setDrawInside(false)
                    val xLabel = ArrayList<String>()
                    xLabel.add(arrayList.get(0).day)
                    xLabel.add(arrayList.get(1).day)
                    xLabel.add(arrayList.get(2).day)
                    xLabel.add(arrayList.get(3).day)
                    xLabel.add(arrayList.get(4).day)
                    xLabel.add(arrayList.get(5).day)
                    xLabel.add(arrayList.get(6).day)
                    val xAxis: XAxis = mChart!!.getXAxis()
                    xAxis.position = XAxis.XAxisPosition.BOTTOM

                    xAxis.granularity = 1f

                    var formatter: ValueFormatter = object : ValueFormatter() {
                        override fun getFormattedValue(value: Float): String {
                            return xLabel.get(value.toInt() % xLabel.size)
                        }
                    }
                    xAxis.valueFormatter = formatter
                    val data = CombinedData()
                    val lineDatas = LineData()
                    val rightAxis: YAxis = mChart!!.getAxisRight()
                    rightAxis.setDrawGridLines(false)
                    rightAxis.axisMinimum = 0f
                    //                    rightAxis.axisMaximum = data.yMax + 9f
                    val leftAxis: YAxis = mChart!!.getAxisLeft()
                    leftAxis.setDrawGridLines(false)
                    leftAxis.axisMinimum = 0f
                    //                    leftAxis.axisMaximum = data.yMax+10f
//                    lineDatas.addDataSet(dataLineChart() as ILineDataSet)
                    data.setData(dataLineChart())
                    data.setData(dataBarChart())
                    xAxis.axisMinimum = data.xMin - 0.5f
                    xAxis.axisMaximum = data.xMax + 0.5f
                    mChart?.setData(data)
                    mChart?.invalidate()

                }, {
                    Log.d("===", "loadData2: ${it.message}")
                })
        )
    }

    private fun dataLineChart(): LineData {
        val d = LineData()
        val data = doubleArrayOf(1.0, 2.0, 2.0, 1.0, 1.0, 1.0, 2.0)
        for (i in data.indices) {
            data[i] = arrayList.get(i).temp
        }
        val entries = ArrayList<Entry>()
        for (index in 0..6) {
            entries.add(
                Entry(
                    index.toFloat(),
                    data[index].toFloat()
                )
            )
//                        entries.add(new Entry(index,data ));
        }
        val set = LineDataSet(entries, "nhiệt độ các ngày")
        set.color = Color.BLUE
        set.lineWidth = 2.5f
        set.setCircleColor(Color.RED)
        set.circleRadius = 3f
        set.fillColor = Color.BLUE
        set.mode = LineDataSet.Mode.CUBIC_BEZIER
        set.setDrawValues(true)
        set.valueTextSize = 10f
        set.valueTextColor = Color.BLUE
        set.axisDependency = YAxis.AxisDependency.LEFT

        d.addDataSet(set)
        return d
    }
    private fun dataBarChart(): BarData {

        val data = arrayListOf("","","","","","","")
        for (i in data.indices) {
            data[i] = arrayList.get(i).humidity
        }
        val entries : ArrayList<BarEntry> = arrayListOf()
        for (index in 0..6) {
            entries.add(
                BarEntry(
                    index.toFloat(),
                    data[index].toFloat()
                )
            )
//                        entries.add(new Entry(index,data ));
        }
        val set1 = BarDataSet(entries,"Độ ẩm từng ngày")
        set1.valueTextSize = 10f
        set1.valueTextColor = Color.BLUE
        set1.barBorderWidth = 1f
        val d = BarData(set1)
        return d
    }

    override fun onValueSelected(e: Entry?, h: Highlight?) {

    }

    override fun onNothingSelected() {

    }
}