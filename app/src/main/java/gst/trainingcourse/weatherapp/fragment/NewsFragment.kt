package gst.trainingcourse.weatherapp.fragment

import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.recyclerview.widget.LinearLayoutManager
import gst.trainingcourse.weatherapp.models_news.News
import gst.trainingcourse.weatherapp.news.NewsAdapter
import gst.trainingcourse.weatherapp.news.XMLDOMParser
import kotlinx.android.synthetic.main.fragment_news.*
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.NodeList
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStreamReader
import java.net.MalformedURLException
import java.net.URL
import java.util.regex.Pattern
import android.view.inputmethod.InputMethodManager
import gst.trainingcourse.weatherapp.NewsDetailActivity
import gst.trainingcourse.weatherapp.R

private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"
class NewsFragment() : Fragment(), NewsAdapter.OnItemClickListener{
    private var param1: String? = null
    private var param2: String? = null
    var URL_NEWS: String = "https://vnexpress.net/rss/tin-moi-nhat.rss"
    private var listNews: MutableList<News> = mutableListOf()
    private var listNewsSearch: MutableList<News> = mutableListOf()
    lateinit var newsAdapter: NewsAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var view = inflater.inflate(R.layout.fragment_news, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ReadRSS().execute(URL_NEWS)
        newsAdapter = NewsAdapter(listNews, view.context, this)
        news_listNews_rv.layoutManager = LinearLayoutManager(context)
        news_listNews_rv.adapter = newsAdapter

        news_nameSearch_et.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, start: Int, cout: Int, after: Int) {
            }
            override fun onTextChanged(charSequence: CharSequence, start: Int, cout: Int, after: Int) {
                searchNews(charSequence.toString())
            }
            override fun afterTextChanged(editable: Editable) {
            }
        })

        news_search_btn.setOnClickListener(){
            val inputManager: InputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        }
    }

    private fun searchNews(name: String){
        if(name == null){
            listNews = listNewsSearch
        }else{
            listNews.clear()
            for(news in listNewsSearch){
                if(news.getTitle().contains(name)){
                    listNews.add(news)
                }
            }
            newsAdapter.notifyDataSetChanged()
        }
    }

    companion object {
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            NewsFragment().apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }

    override fun OnItemClick(position: Int) {
        var intent = Intent(context, NewsDetailActivity::class.java)
        intent.putExtra("TITLE", listNews[position].getTitle())
        intent.putExtra("LINK", listNews[position].getLink())
        intent.putExtra("IMG", listNews[position].getImgSrc())
        startActivity(intent)
    }
    inner class ReadRSS: AsyncTask<String, Void, String>(){
        override fun onPreExecute() {
            super.onPreExecute()
            Toast.makeText(context, "Loading...", Toast.LENGTH_SHORT).show()
        }
        override fun doInBackground(vararg params: String?): String {
            var content: StringBuilder = StringBuilder()
            try {
                var url: URL = URL(params[0])
                var inputStreamReader: InputStreamReader = InputStreamReader(url.openConnection().getInputStream())
                var bufferedReader: BufferedReader = BufferedReader(inputStreamReader)
                var listLine: List<String> = bufferedReader.readLines()
                for (i in 0..listLine.size - 1){
                    content.append(listLine[i])
                }
                bufferedReader.close()
            }catch (e: MalformedURLException){
                e.printStackTrace()
            }catch (e: IOException){
                e.printStackTrace()
            }
            return content.toString()
        }
        override fun onPostExecute(result: String) {
            super.onPostExecute(result)
            var xmldomParser: XMLDOMParser = XMLDOMParser()
            var document: Document = xmldomParser.getDocument(result)
            var nodeList: NodeList = document!!.getElementsByTagName("item")
            var nodeListImg: NodeList = document!!.getElementsByTagName("description")
            for(i in 0 until nodeList.length) {
                var element: Element = nodeList.item(i) as Element
                var title: String = xmldomParser.getValue(element, "title")
                var img = ""
                var link: String = xmldomParser.getValue(element, "link")
                var cData = nodeListImg.item(i + 1).textContent
                var pattern = Pattern.compile("<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>")
                var matcher = pattern.matcher(cData)
                if(matcher.find()){
                    img = matcher.group(1)
                    var news = News(title, img, link)
                    listNews.add(news)
                    listNewsSearch.add(news)
                }
            }
            newsAdapter.notifyDataSetChanged()
        }
    }
}