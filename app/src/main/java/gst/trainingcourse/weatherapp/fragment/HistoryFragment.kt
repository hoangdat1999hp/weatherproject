package gst.trainingcourse.weatherapp.fragment

import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.LinearLayoutManager
import gst.trainingcourse.weatherapp.HistoryAdapter
import gst.trainingcourse.weatherapp.ItemClickOn
import gst.trainingcourse.weatherapp.R
import gst.trainingcourse.weatherapp.roomdb.Cityroom
import gst.trainingcourse.weatherapp.roomdb.IDaoroom
import gst.trainingcourse.weatherapp.roomdb.Roomdb
import kotlinx.android.synthetic.main.fragment_history.*
//import kotlinx.android.synthetic.main.fragment_history.etfindcity
import kotlinx.android.synthetic.main.fragment_history.view.*
import kotlinx.android.synthetic.main.fragment_history.view.findcity1
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.util.*

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HistoryFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HistoryFragment : Fragment() {
    private lateinit var adapter : HistoryAdapter
    private var listcity :MutableList<Cityroom> = mutableListOf()
    private var roomDao: IDaoroom?=null
    
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view = inflater.inflate(R.layout.fragment_history, container, false)
        roomDao = Roomdb.getAppdatabase((view.context))!!.cityroomDAO()!!
        listcity = (roomDao!!.getalldb())!!
        var search = view.findViewById<ImageButton>(R.id.findcity1)
        var cityname = view.findViewById<EditText>(R.id.etfindcity)
        var layoutManager2= LinearLayoutManager(view.context)
        Log.d("====","${layoutManager2}")
        view.recycler2.layoutManager = layoutManager2
        adapter = HistoryAdapter(listcity,view.context)
        view.recycler2.adapter = adapter

        adapter.notifyDataSetChanged()
        adapter!!.itemClickOn = object : ItemClickOn {
            @RequiresApi(Build.VERSION_CODES.O)
            override fun onClick(postion: Int) {
                requireActivity().supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, HomeFragment(listcity!!.get(postion).name)).commit()

            }

        }

        search.setOnClickListener{
            val name = cityname.text.toString()
            listcity!!.clear()
            listcity = (roomDao!!.gethistoryByName(name))!!
//            Log.d("listcity", listcity!!.get(0).name)
            adapter = HistoryAdapter(listcity,view.context)
            view.recycler2.adapter = adapter
        }
        return view
    }
    private fun getData(){
        listcity = (roomDao!!.getalldb())!!
        adapter!!.notifyDataSetChanged()
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HistoryFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HistoryFragment().apply {

            }
    }


}