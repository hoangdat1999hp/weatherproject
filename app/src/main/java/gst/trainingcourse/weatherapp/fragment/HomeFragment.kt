package gst.trainingcourse.weatherapp.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.AlarmManager
import android.app.AlertDialog
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.*
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.github.mikephil.charting.charts.CombinedChart
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.squareup.picasso.Picasso
import gst.trainingcourse.weatherapp.*
import gst.trainingcourse.weatherapp.models.Custom_gio
import gst.trainingcourse.weatherapp.models.Custom_ngay
import gst.trainingcourse.weatherapp.models.We
import gst.trainingcourse.weatherapp.models.dailyWeather
import gst.trainingcourse.weatherapp.models2.Wex
import gst.trainingcourse.weatherapp.roomdb.Cityroom
import gst.trainingcourse.weatherapp.roomdb.IDaoroom
import gst.trainingcourse.weatherapp.roomdb.Roomdb
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import java.text.Normalizer
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import kotlin.math.round
import kotlin.properties.Delegates


// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@RequiresApi(Build.VERSION_CODES.O)
class HomeFragment(var name:String) : Fragment() , LocationListener {
    // TODO: Rename and change types of parameters
    private var param1: String? = null
    private var param2: String? = null
    private val mCompositeDisposable = CompositeDisposable()
    private var we: We?=null
    private var wex: Wex?=null
    private var adapter: TimeAdaper?=null
    private var adapter2: DayAdapter?=null
    private var customGio:MutableList<Custom_gio> = mutableListOf()
    private var customNgay:MutableList<Custom_ngay> = mutableListOf()
    private var arrayList: ArrayList<dailyWeather> = arrayListOf()
    private lateinit var locationManager: LocationManager
    private val locationPermissionCode = 2
    var cityname = name
    var icon = ""
    var mdate = ""
    var mtime = ""
    var t = ""
    var setScheduled : Boolean = false
    lateinit var cal : Calendar
    private var mAddHis by Delegates.notNull<Boolean>()
    private var mChart: CombinedChart? = null
    var lat : Double = 0.0
    var lng : Double = 0.0
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()
    private val WHITESPACE_REGEX = "\\s".toRegex()
    private var roomDao: IDaoroom?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mAddHis = false
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(requireContext())
        cal = Calendar.getInstance()
        cal[Calendar.HOUR_OF_DAY] = 9
        cal[Calendar.MINUTE] = 16
        cal[Calendar.SECOND] = 0

    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_home, container, false)
        val view: View = inflater!!.inflate(R.layout.fragment_home, container, false)
        checkPer()
        view.findcity.setOnClickListener { view ->
//            etfindcity.visibility = EditText.VISIBLE
            var city = etfindcity.text.toString()

            if(city.equals("")){
                city = cityname!!
//                loadData(this,city)
            }
            else{
                mAddHis = true
                cityname = city
                customGio.clear()
                arrayList.clear()
                customNgay.clear()
                getWeatherTime(requireContext(),cityname!!)
                getWeatherDay(requireContext(), cityname!!)
                etfindcity.setText("")
                sendDataToActivity(cityname)

            }
//            mAddHis = false
        }
        view.here.setOnClickListener {
            checkPer()
        }
//        view.all.setOnClickListener {
//            etfindcity.visibility = EditText.GONE
//            etfindcity.setText("")
//        }
        roomDao = Roomdb.getAppdatabase((view.context))!!.cityroomDAO()!!
        return view
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment("").apply {
                arguments = Bundle().apply {
                    putString(ARG_PARAM1, param1)
                    putString(ARG_PARAM2, param2)
                }
            }
    }
    private fun checkPer(){
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPer()
        }
        else {
            getGPS()
        }
    }
    private fun requestPer(){
        ActivityCompat.requestPermissions(
            requireActivity(),
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ), 100
        )
    }
    @SuppressLint("MissingPermission")
    private fun getlocation(){
        Log.d("--", "getlocation ")
        locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            getGPS()
        }
        else{
            getWeatherDay(requireContext(), cityname!!)
            getWeatherTime(requireContext(), cityname!!)
        }
    }
    @SuppressLint("MissingPermission")
    private fun getGPS(){
        locationManager = activity?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
        fusedLocationClient.lastLocation
            .addOnSuccessListener { location: Location? ->
                if (location == null){
                    getlocation()
                }
                else{
                    Log.d("--", "getGPS ")
                    customGio.clear()
                    arrayList.clear()
                    customNgay.clear()
                    lat = location?.latitude!!
                    lng = location?.longitude!!
                    Log.d("lat", location?.latitude.toString())
                    Log.d("lon", location?.longitude.toString())
                    cityname = getCity(lat, lng)
                    Log.d("=+", "${cityname}")
                    getWeatherTime(requireContext(), cityname!!)
                    getWeatherDay(requireContext(),cityname!!)
                }
//                    val intent = Intent(requireContext(), MyService::class.java)
//                    intent.putExtra("cityname", cityname)
//                    val pintent = PendingIntent.getService(requireContext(), 0, intent, 0)
//                    val alarm =
//                        requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager?
//                    alarm!!.setRepeating(
//                        AlarmManager.RTC_WAKEUP,
//                        cal.getTimeInMillis(),
//                        (24 * 60 * 60 * 1000).toLong(),
//                        pintent
//                    )

            }
    }


    private fun showAlert() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle("Cannot detect yor address")
        builder.setMessage("GPS need to be enabled")
        builder.setPositiveButton("OK", null
//            {dialog, which -> finish()}
        )
        val dialog = builder.create()
        dialog.show()
    }
    private fun getCity(lat: Double, lng: Double):String {
        val gcd = Geocoder(requireContext(), Locale.getDefault())
        var s = ""

        if (name !=""){
            s = name
//            setScheduled = false
        }
        else {
            val addresses: List<Address> = gcd.getFromLocation(lat, lng, 1)
            if (addresses.size > 0) {
                s = (addresses.get(0).adminArea).unaccent()
                s = s.replace(WHITESPACE_REGEX, "")
                Log.d("city", s)
            }
        }
        sendDataToActivity(s)

        return s
    }
    fun CharSequence.unaccent(): String {
        val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
        return REGEX_UNACCENT.replace(temp, "")
    }


    fun getWeatherDay(context: Context, cityname: String) {
        loadData2(context, cityname)
        var layoutManager2 = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        theongay.layoutManager = layoutManager2
        adapter2 = DayAdapter(customNgay,context)
        theongay.adapter = adapter2
    }
    @RequiresApi(Build.VERSION_CODES.O)
    fun getWeatherTime(context: Context, cityname: String){
        loadData(context, cityname)
        var layoutManager1 = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        theogio.layoutManager = layoutManager1
        adapter = TimeAdaper(customGio, context)
        theogio.adapter = adapter
    }
    private fun addToHistory(cityname: String){
        var cityroom = Cityroom(0,cityname,icon,mdate,mtime,t)
        Log.d("degree", t)
        roomDao!!.addhistory(cityroom)
    }
    @RequiresApi(Build.VERSION_CODES.O)
    private fun loadData(context: Context, cityname: String) {
        val x :String = "1"
        mCompositeDisposable.add(
            RetrofitClient.getAPI().getData(cityname).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    cityShow.text = it.city.name
                    weather.text = it.list.get(0).weather.get(0).main
                    celsiusmax.text = round(it.list.get(0).main.temp_max).toInt().toString()
                    celsiusmin.text = round(it.list.get(0).main.temp_min).toInt().toString()
                    t = round(it.list.get(0).main.temp).toInt().toString()
                    Log.d("degree", t)
                    pcloud.text = it.list.get(0).clouds.all.toString()+ "%"
                    tocdogio.text = it.list.get(0).wind.speed.toString() +"m/s"
                    doAm.text = it.list.get(0).main.humidity.toString() +"%"
                    var datew = it.list.get(0).dt
                    val date = Date(datew * 1000L)
                    val simpleDateFormat = SimpleDateFormat("yyyy/MM/dd")
                    val simpleDateFormat2 = SimpleDateFormat("HH:mm")
                    val currentDateTime = LocalDateTime.now()
                    icon = it.list.get(0).weather.get(0).icon
//                    mtime = currentDateTime.format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))
                    val timeInMillis = System.currentTimeMillis();
                    val cal1 = Calendar.getInstance();
                    cal1.setTimeInMillis(timeInMillis);
                    val dateFormat = SimpleDateFormat("HH:mm");
                    mtime = dateFormat.format(cal1.getTime())
                    mdate = simpleDateFormat.format(date)
                    Log.d("====","hhh")
//                    var cityroom = Cityroom(0,it.city.name,it.list.get(0).weather.get(0).icon,mdate,mtime,t)
                    if(mAddHis){
                        addToHistory(it.city.name)
                    }



                    Picasso.with(context).load(
                        "${RetrofitClient.URL_THUMBNAIL}${
                            it.list.get(0).weather.get(
                                0
                            ).icon
                        }" + ".png"
                    )
                        .into(imagewerther)
                    for (i in it.list) {
                        val l: Int = i.dt
                        val date = Date(l * 1000L)
                        val simpleDateFormat = SimpleDateFormat("HH:mm")
                        val Day = simpleDateFormat.format(date)
                        var gio = Day
                        var mua = (i.main.humidity).toString() + "%"
                        var temp = round(i.main.temp_max).toInt().toString()
                        var icon = i.weather.get(0).icon
                        var customGiox = Custom_gio(gio, mua, temp, icon)
                        customGio!!.add(customGiox)
                    }
                    adapter!!.notifyDataSetChanged()

                    Log.d("==", "${it.list.get(0).dt_txt}")

                }, {
                    Log.d("==", "loadData: ${it.message}")
                })

        )

    }
    //    truyen data de hien thi thong bao chuẩn
    fun passDatatoService(celsiusmin : String, celsiusmax : String, wether: String, cityname: String ){
        var activity = activity
        var intent : Intent = Intent(activity, MyService::class.java)
        intent.putExtra("nhietdomin",celsiusmin)
        intent.putExtra("nhietdomax",celsiusmax)
        intent.putExtra("wether",wether)
        intent.putExtra("cityname",cityname)


        activity!!.startForegroundService(intent)


    }

    private fun loadData2(context: Context, cityname: String) {
        mCompositeDisposable.add(
            RetrofitClient.getAPI2().getData2(cityname).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    wex = it
                    for (i in 0..6) {
                        val humidity = it.list.get(i).humidity.toString()
                        var thu = it.list.get(i).dt
                        val date = Date(thu * 1000L)
                        val simpleDateFormat = SimpleDateFormat("EEEE")
                        val day = simpleDateFormat.format(date)
                        val mua = it.list.get(i).weather.get(0).icon
                        val ndmax = round(it.list.get(i).temp.max).toInt().toString()
                        val ndmin = round(it.list.get(i).temp.min).toInt().toString()
                        val main = round(it.list.get(i).temp.day).toInt().toString()
                        val customNgay1 = Custom_ngay(day,mua,ndmax,ndmin,humidity)
                        customNgay.add(customNgay1)
//                        arrayList.add(dailyWeather(day, main))
                    }
//                    mChart = findViewById<View>(R.id.linechart) as CombinedChart
//                    mChart?.getDescription()?.setEnabled(false)
//                    mChart?.setBackgroundColor(Color.DKGRAY)
//                    mChart?.setDrawGridBackground(false)
//
//                    mChart?.setDrawBarShadow(false);
//                    mChart?.setHighlightFullBarEnabled(false);
//                    mChart?.setOnChartValueSelectedListener(this)
//
//                    val xLabel = ArrayList<String>()
//                    xLabel.add(arrayList.get(0).day)
//                    xLabel.add(arrayList.get(1).day)
//                    xLabel.add(arrayList.get(2).day)
//                    xLabel.add(arrayList.get(3).day)
//                    xLabel.add(arrayList.get(4).day)
//                    xLabel.add(arrayList.get(5).day)
//                    xLabel.add(arrayList.get(6).day)
//                    val xAxis: XAxis = mChart!!.getXAxis()
//                    xAxis.position = XAxis.XAxisPosition.BOTTOM
//                    xAxis.axisMinimum = 0f
//                    xAxis.granularity = 1f
//
//                    var formatter: ValueFormatter = object : ValueFormatter() {
//                        override fun getFormattedValue(value: Float): String {
//                            return xLabel.get(value.toInt() % xLabel.size)
//                        }
//                    }
//                    xAxis.valueFormatter = formatter
//                    val data = CombinedData()
//                    val lineDatas = LineData()
//                    val rightAxis: YAxis = mChart!!.getAxisRight()
//                    rightAxis.setDrawGridLines(false)
//                    rightAxis.axisMinimum = 0f
//                    //                    rightAxis.axisMaximum = data.yMax + 9f
//                    val leftAxis: YAxis = mChart!!.getAxisLeft()
//                    leftAxis.setDrawGridLines(false)
//                    leftAxis.axisMinimum = 0f
//                    //                    leftAxis.axisMaximum = data.yMax+10f
//                    lineDatas.addDataSet(dataChart() as ILineDataSet)
//                    data.setData(lineDatas)
//                    xAxis.axisMaximum = data.xMax + 0.25f
//                    mChart?.setData(data)
//                    mChart?.invalidate()
//                    Log.d("===", "${customNgay.get(0).ndmax}")
                    adapter2!!.notifyDataSetChanged()

                    Log.d("===", "LL")
                }, {
                    Log.d("===", "loadData2: ${it.message}")
                })
        )
    }

    override fun onLocationChanged(location: Location) {
        customGio.clear()
        arrayList.clear()
        customNgay.clear()
        cityname = getCity(location.latitude, location.longitude)
        getWeatherTime(requireContext(), cityname!!)
        getWeatherDay(requireContext(),cityname!!)
        locationManager.removeUpdates(this)

    }

    override fun onProviderDisabled(provider: String) {
        showAlert()
    }

    override fun onProviderEnabled(provider: String) {
//        getlocation()
    }
    private fun sendDataToActivity(cityname: String){
        val intent = Intent("getCity")
        intent.putExtra("city",cityname)
        LocalBroadcastManager.getInstance(requireContext()).sendBroadcast(intent)
    }
}