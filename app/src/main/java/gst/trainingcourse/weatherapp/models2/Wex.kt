package gst.trainingcourse.weatherapp.models2


data class Wex(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: MutableList<Aweatherx>,
    val message: Double
)