package gst.trainingcourse.weatherapp.models2

data class Coord(
    val lat: Double,
    val lon: Double
)