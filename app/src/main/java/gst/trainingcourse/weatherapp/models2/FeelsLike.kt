package gst.trainingcourse.weatherapp.models2

data class FeelsLike(
    val day: Double,
    val eve: Double,
    val morn: Double,
    val night: Double
)