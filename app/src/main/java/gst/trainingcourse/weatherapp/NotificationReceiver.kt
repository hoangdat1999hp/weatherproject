package gst.trainingcourse.weatherapp

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.os.Build
import android.util.Log
import android.widget.Toast
import androidx.annotation.RequiresApi


class NotificationReceiver : BroadcastReceiver() {
    private val TAG :String = "TAG"
    val ACTION_RUN : String = "ACTION_RUN"
    @RequiresApi(Build.VERSION_CODES.O)
    override fun onReceive(context: Context, intent: Intent?) {
        if (intent != null) {
            if (intent.action.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
                if (context!=null){

                    val networkInfo: NetworkInfo? =
                        intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO)
                    if (networkInfo != null && networkInfo.detailedState == NetworkInfo.DetailedState.CONNECTED) {
                        Toast.makeText(context, "Có wifi", Toast.LENGTH_SHORT).show()
                        Log.d(TAG, "onReceive: Da có ketnoi")
                        val serviceIntent = Intent(context, MyService::class.java)
//                        context.startForegroundService(serviceIntent)
                    } else if (networkInfo != null && networkInfo.detailedState == NetworkInfo.DetailedState.DISCONNECTED) {
                        Toast.makeText(context, "Không có wifi", Toast.LENGTH_SHORT).show()
                    }

                }
            }
        }
    }



}