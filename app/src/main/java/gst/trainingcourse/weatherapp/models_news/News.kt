package gst.trainingcourse.weatherapp.models_news

class News(private var mTilte: String, private var mImgSrc: String, private var mLink: String) {
    fun getTitle(): String{
        return this.mTilte
    }
    fun getImgSrc(): String{
        return this. mImgSrc
    }
    fun getLink(): String{
        return this.mLink
    }
}