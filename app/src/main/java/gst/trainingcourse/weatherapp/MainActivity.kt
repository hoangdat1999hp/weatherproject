//package gst.trainingcourse.weatherapp
//
////import com.github.mikephil.charting.formatter.ValueFormatter
//
//import android.Manifest
//import android.annotation.SuppressLint
//import android.app.AlertDialog
//import android.content.Context
//import android.content.pm.PackageManager
//import android.graphics.Color
//import android.location.*
//import android.os.Bundle
//import android.util.Log
//import android.view.View
//import android.widget.EditText
//import androidx.appcompat.app.AppCompatActivity
//import androidx.core.app.ActivityCompat
//import androidx.fragment.app.Fragment
//import androidx.recyclerview.widget.LinearLayoutManager
//import com.github.mikephil.charting.charts.CombinedChart
//import com.github.mikephil.charting.components.XAxis
//import com.github.mikephil.charting.components.YAxis
//import com.github.mikephil.charting.data.*
//import com.github.mikephil.charting.formatter.ValueFormatter
//import com.github.mikephil.charting.highlight.Highlight
//import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
//import com.github.mikephil.charting.listener.OnChartValueSelectedListener
//import com.google.android.gms.location.FusedLocationProviderClient
//import com.google.android.gms.location.LocationServices
//import com.google.android.material.bottomnavigation.BottomNavigationView
//import com.squareup.picasso.Picasso
//import gst.trainingcourse.weatherapp.models.Custom_gio
//import gst.trainingcourse.weatherapp.models.Custom_ngay
//import gst.trainingcourse.weatherapp.models.We
//import gst.trainingcourse.weatherapp.models.dailyWeather
//import gst.trainingcourse.weatherapp.models2.Wex
//import io.reactivex.android.schedulers.AndroidSchedulers
//import io.reactivex.disposables.CompositeDisposable
//import io.reactivex.schedulers.Schedulers
//import kotlinx.android.synthetic.main.activity_main.*
//import java.text.Normalizer
//import java.text.SimpleDateFormat
//import java.util.*
//import kotlin.collections.ArrayList
//import kotlin.math.round
//
//
//class MainActivity : AppCompatActivity(),OnChartValueSelectedListener, LocationListener {
//    private val mCompositeDisposable = CompositeDisposable()
//    private var we: We?=null
//    private var wex: Wex?=null
//    private var adapter:TimeAdaper?=null
//    private var adapter2:DayAdapter?=null
//    lateinit var context: Context
//    private var customGio:MutableList<Custom_gio> = mutableListOf()
//    private var customNgay:MutableList<Custom_ngay> = mutableListOf()
//    private var arrayList: ArrayList<dailyWeather> = arrayListOf()
//    private lateinit var locationManager: LocationManager
//    private val locationPermissionCode = 2
//    var cityname = "london"
//    private var mChart: CombinedChart? = null
//    var lat : Double = 0.0
//    var lng : Double = 0.0
//    private lateinit var fusedLocationClient: FusedLocationProviderClient
//    private val REGEX_UNACCENT = "\\p{InCombiningDiacriticalMarks}+".toRegex()
//    private val WHITESPACE_REGEX = "\\s".toRegex()
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//        context = this
//        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
//        checkPer()
//        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
//        bottomNavigationView.setOnNavigationItemSelectedListener(onNav)
//        findcity.setOnClickListener {
//            etfindcity.visibility = EditText.VISIBLE
//            var city = etfindcity.text.toString()
//            if(city.equals("")){
//                city = cityname!!
////                loadData(this,city)
//            }
//            else{
//                cityname = city
//                customGio.clear()
//                arrayList.clear()
//                getWeatherTime(this,cityname!!)
//                customNgay.clear()
//                getWeatherDay(this, cityname!!)
//            }
//        }
//        here.setOnClickListener {
//            checkPer()
//        }
//        all.setOnClickListener {
//            etfindcity.visibility = EditText.GONE
//            etfindcity.setText("")
//        }
//    }
//    private fun checkPer(){
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_COARSE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//            requestPer()
//        }
//        else {
//            getGPS()
//        }
//    }
//    private fun requestPer(){
//        ActivityCompat.requestPermissions(
//            this,
//            arrayOf(
//                Manifest.permission.ACCESS_FINE_LOCATION,
//                Manifest.permission.ACCESS_COARSE_LOCATION
//            ), 100
//        )
//    }
//    @SuppressLint("MissingPermission")
//    private fun getlocation(){
//        Log.d("--", "getlocation ")
//        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
//
//            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 5f, this)
//
//    }
//
//    override fun onRequestPermissionsResult(
//        requestCode: Int,
//        permissions: Array<out String>,
//        grantResults: IntArray
//    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
//        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            getGPS()
//        }
//        else{
//            getWeatherDay(this, cityname!!)
//            getWeatherTime(this, cityname!!)
//        }
//    }
//    @SuppressLint("MissingPermission")
//    private fun getGPS(){
//        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
//        fusedLocationClient.lastLocation
//            .addOnSuccessListener { location: Location? ->
//                if (location == null){
//                    getlocation()
//                }
//                else{
//                    Log.d("--", "getGPS ")
//                    lat = location?.latitude!!
//                    lng = location?.longitude!!
//                    Log.d("lat", location?.latitude.toString())
//                    Log.d("lon", location?.longitude.toString())
//                    cityname = getCity(lat, lng)
//                    getWeatherTime(this, cityname!!)
//                    getWeatherDay(this,cityname!!)
//                }
//
//            }
//    }
//
//
//    private fun showAlert() {
//        val builder = AlertDialog.Builder(this)
//        builder.setTitle("Cannot detect yor address")
//        builder.setMessage("GPS need to be enabled")
//        builder.setPositiveButton("OK", null
////            {dialog, which -> finish()}
//        )
//        val dialog = builder.create()
//        dialog.show()
//    }
//     private fun getCity(lat: Double, lng: Double):String {
//        val gcd = Geocoder(this, Locale.getDefault())
//         var s = ""
//        val addresses: List<Address> = gcd.getFromLocation(lat, lng, 1)
//        if (addresses.size > 0) {
//            s = (addresses.get(0).adminArea).unaccent()
//            s = s.replace(WHITESPACE_REGEX, "")
//            Log.d("city", s)
//        }
//         return s
//    }
//    fun CharSequence.unaccent(): String {
//        val temp = Normalizer.normalize(this, Normalizer.Form.NFD)
//        return REGEX_UNACCENT.replace(temp, "")
//    }
//
//
//    fun getWeatherDay(context: Context, cityname: String) {
//        loadData2(context, cityname)
//        var layoutManager2 = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        theongay.layoutManager = layoutManager2
//        adapter2 = DayAdapter(customNgay,context)
//        theongay.adapter = adapter2
//    }
//    fun getWeatherTime(context: Context, cityname: String){
//        loadData(context, cityname)
//        var layoutManager1 = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
//        theogio.layoutManager = layoutManager1
//        adapter = TimeAdaper(customGio, context)
//        theogio.adapter = adapter
//    }
//    private fun loadData(context: Context, cityname: String) {
//        val x :String = "1"
//        mCompositeDisposable.add(
//            RetrofitClient.getAPI().getData(cityname).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    cityShow.text = it.city.name
//                    weather.text = it.list.get(0).weather.get(0).description
//                    celsiusmax.text = round(it.list.get(0).main.temp_max).toInt().toString()
//                    celsiusmin.text = round(it.list.get(0).main.temp_min).toInt().toString()
//                    pcloud.text = it.list.get(0).clouds.all.toString()+ "%"
//                    tocdogio.text = it.list.get(0).wind.speed.toString() +"m/s"
//                    doAm.text = it.list.get(0).main.humidity.toString() +"%"
//                    Picasso.with(context).load(
//                        "${RetrofitClient.URL_THUMBNAIL}${
//                            it.list.get(0).weather.get(
//                                0
//                            ).icon
//                        }" + ".png"
//                    )
//                        .into(imagewerther)
//                    for (i in it.list) {
//                        val l: Int = i.dt
//                        val date = Date(l * 1000L)
//                        val simpleDateFormat = SimpleDateFormat("HH:mm")
//                        val Day = simpleDateFormat.format(date)
//                        var gio = Day
//                        var mua = (i.main.humidity).toString() + "%"
//                        var temp = round(i.main.temp_max).toInt().toString()
//                        var icon = i.weather.get(0).icon
//                        var customGiox = Custom_gio(gio, mua, temp, icon)
//                        customGio!!.add(customGiox)
//                    }
//                    adapter!!.notifyDataSetChanged()
//                    Log.d("==", "${it.list.get(0).dt_txt}")
//                }, {
//                    Log.d("==", "loadData: ${it.message}")
//                })
//        )
//    }
//    private fun loadData2(context: Context, cityname: String) {
//        mCompositeDisposable.add(
//            RetrofitClient.getAPI2().getData2(cityname).subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    wex = it
//                    for (i in 0..6) {
//                        var thu = it.list.get(i).dt
//                        val date = Date(thu * 1000L)
//                        val simpleDateFormat = SimpleDateFormat("EEEE")
//                        val day = simpleDateFormat.format(date)
//                        val mua = it.list.get(i).weather.get(0).icon
//                        val ndmax = round(it.list.get(i).temp.max).toInt().toString()
//                        val ndmin = round(it.list.get(i).temp.min).toInt().toString()
//                        val main = round(it.list.get(i).temp.day).toInt().toString()
//                        val customNgay1 = Custom_ngay(day,mua,ndmax,ndmin)
//                        customNgay.add(customNgay1)
//                        arrayList.add(dailyWeather(day, main))
//                    }
//                    mChart = findViewById<View>(R.id.linechart) as CombinedChart
//                    mChart?.getDescription()?.setEnabled(false)
//                    mChart?.setBackgroundColor(Color.DKGRAY)
//                    mChart?.setDrawGridBackground(false)
//
//                    mChart?.setDrawBarShadow(false);
//                    mChart?.setHighlightFullBarEnabled(false);
//                    mChart?.setOnChartValueSelectedListener(this)
//
//                    val xLabel = ArrayList<String>()
//                    xLabel.add(arrayList.get(0).day)
//                    xLabel.add(arrayList.get(1).day)
//                    xLabel.add(arrayList.get(2).day)
//                    xLabel.add(arrayList.get(3).day)
//                    xLabel.add(arrayList.get(4).day)
//                    xLabel.add(arrayList.get(5).day)
//                    xLabel.add(arrayList.get(6).day)
//                    val xAxis: XAxis = mChart!!.getXAxis()
//                    xAxis.position = XAxis.XAxisPosition.BOTTOM
//                    xAxis.axisMinimum = 0f
//                    xAxis.granularity = 1f
//
//                    var formatter: ValueFormatter = object : ValueFormatter() {
//                        override fun getFormattedValue(value: Float): String {
//                            return xLabel.get(value.toInt() % xLabel.size)
//                        }
//                    }
//                    xAxis.valueFormatter = formatter
//                    val data = CombinedData()
//                    val lineDatas = LineData()
//                    val rightAxis: YAxis = mChart!!.getAxisRight()
//                    rightAxis.setDrawGridLines(false)
//                    rightAxis.axisMinimum = 0f
//                    //                    rightAxis.axisMaximum = data.yMax + 9f
//                    val leftAxis: YAxis = mChart!!.getAxisLeft()
//                    leftAxis.setDrawGridLines(false)
//                    leftAxis.axisMinimum = 0f
//                    //                    leftAxis.axisMaximum = data.yMax+10f
//                    lineDatas.addDataSet(dataChart() as ILineDataSet)
//                    data.setData(lineDatas)
//                    xAxis.axisMaximum = data.xMax + 0.25f
//                    mChart?.setData(data)
//                    mChart?.invalidate()
//                    Log.d("===", "${customNgay.get(0).ndmax}")
//                    adapter2!!.notifyDataSetChanged()
//
//                    Log.d("===", "LL")
//                }, {
//                    Log.d("===", "loadData2: ${it.message}")
//                })
//        )
//    }
////     fun loadChart(context: Context, cityname: String){
////        mCompositeDisposable.add(
////            RetrofitClient.getAPI2().getData2(cityname).subscribeOn(Schedulers.io())
////                .observeOn(AndroidSchedulers.mainThread())
////                .subscribe {
////                    for (i in 0..6) {
////                        var thu = it.list.get(i).dt
////                        val date = Date(thu * 1000L)
////                        val simpleDateFormat = SimpleDateFormat("EEEE")
////                        val Day = simpleDateFormat.format(date)
////                        val main = (it.list.get(i).temp.day - 273.5).toInt().toString()
////                        arrayList.add(dailyWeather(Day, main))
////                    }
////
////                }
////        )
////    }
//    private fun dataChart(): DataSet<*>? {
//        val d = LineData()
//        val data = intArrayOf(1, 2, 2, 1, 1, 1, 2)
//        for (i in data.indices) {
//            data[i] =arrayList.get(i).temp.toInt()
//        }
//        val entries = ArrayList<Entry>()
//        for (index in 0..6) {
//            entries.add(
//                Entry(
//                    index.toFloat(),
//                    data[index].toFloat()
//                )
//            )
////                        entries.add(new Entry(index,data ));
//        }
//        val set = LineDataSet(entries, "nhiệt độ các ngày")
//        set.color = Color.BLUE
//        set.lineWidth = 2.5f
//        set.setCircleColor(Color.BLUE)
//        set.circleRadius = 2f
//        set.fillColor = Color.BLUE
//        set.mode = LineDataSet.Mode.CUBIC_BEZIER
//        set.setDrawValues(true)
//        set.valueTextSize = 10f
//        set.valueTextColor = Color.BLUE
//        set.axisDependency = YAxis.AxisDependency.LEFT
//        d.addDataSet(set)
//        return set
//    }
//
//    override fun onValueSelected(e: Entry?, h: Highlight?) {
//
//    }
//
//    override fun onNothingSelected() {
//
//    }
//
//    override fun onLocationChanged(location: Location) {
//        customGio.clear()
//        arrayList.clear()
//        customNgay.clear()
//        cityname = getCity(location.latitude, location.longitude)
//        getWeatherTime(this, cityname!!)
//        getWeatherDay(this,cityname!!)
//        locationManager.removeUpdates(this)
//
//    }
//
//    override fun onProviderDisabled(provider: String) {
//        showAlert()
//    }
//
//    override fun onProviderEnabled(provider: String) {
////        getlocation()
//    }
//    private val onNav =
//        BottomNavigationView.OnNavigationItemSelectedListener { item ->
//            var selected: Fragment? = null
//            when (item.itemId) {
//                R.id.nav_home -> selected = HomeFragment()
////                R.id.nav_search -> selected = SearchFragment()
////                R.id.nav_add -> SendUserToPostActivity()
////                R.id.nav_profile -> selected = ProfileFragment()
////                R.id.nav_notify -> selected = NotifyFragment()
//            }
//            if (selected != null) {
//                supportFragmentManager.beginTransaction().replace(
//                    R.id.fragment_container,
//                    selected
//                ).commit()
//            }
//            true
//        }
//
//
//
//}