package gst.trainingcourse.weatherapp

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import gst.trainingcourse.weatherapp.models.Custom_ngay

class DayAdapter(var listDay: MutableList<Custom_ngay>, var context: Context) : RecyclerView.Adapter<DayAdapter.ViewHolder>() {
    class ViewHolder(itemview: View):RecyclerView.ViewHolder(itemview) {
        var thu: TextView
        var humidity: TextView
        var ndmax:TextView
        var ndmin:TextView
        var image: ImageView

        init {
            thu = itemView.findViewById(R.id.thu)
            humidity = itemView.findViewById<TextView>(R.id.statusitem)
            ndmax = itemView.findViewById<TextView>(R.id.ndmax)
            ndmin = itemView.findViewById<TextView>(R.id.ndmin)
            image = itemview.findViewById(R.id.image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var v = LayoutInflater.from(parent.context).inflate(R.layout.item_ngay, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        //gán dữ liệu vào textview
        val customNgay: Custom_ngay = listDay.get(position)

        holder.thu.setText(customNgay.thu)
        Picasso.with(context).load(
            "${RetrofitClient.URL_THUMBNAIL}${
                listDay.get(position).mua
            }" + ".png"
        )
            .into(holder.image)
        holder.humidity.text = customNgay.humidity
        holder.ndmax.setText(customNgay.ndmax)
        holder.ndmin.setText(customNgay.ndmin)

    }

    override fun getItemCount(): Int {
       return listDay.size
    }
}