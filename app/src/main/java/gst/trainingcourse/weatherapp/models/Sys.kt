package gst.trainingcourse.weatherapp.models

data class Sys(
    val pod: String
)