package gst.trainingcourse.weatherapp.models

data class Coord(
    val lat: Double,
    val lon: Double
)