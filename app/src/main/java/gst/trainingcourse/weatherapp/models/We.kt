package gst.trainingcourse.weatherapp.models

import gst.trainingcourse.weatherapp.models.City
import gst.trainingcourse.weatherapp.models.Aweather

data class We(
    val city: City,
    val cnt: Int,
    val cod: String,
    val list: MutableList<Aweather>,
    val message: Int
)