package gst.trainingcourse.weatherapp.models

data class Rain(
    val `3h`: Double
)