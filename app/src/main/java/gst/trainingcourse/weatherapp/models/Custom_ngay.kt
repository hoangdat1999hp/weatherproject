package gst.trainingcourse.weatherapp.models

class Custom_ngay {
    var thu: String? = null
    var mua: String? = null
    var humidity: String? = null
    var ndmax: String? = null
    var ndmin: String? = null

    //    private String image;
    constructor(thu: String?, mua: String?, ndmax: String?, ndmin: String?, humidity: String?) {
        this.thu = thu
        this.mua = mua
        this.ndmax = ndmax
        this.ndmin = ndmin
        this.humidity = humidity
    }


}