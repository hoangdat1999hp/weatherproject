package gst.trainingcourse.weatherapp.models

data class Wind(
    val deg: Int,
    val gust: Double,
    val speed: Double
)