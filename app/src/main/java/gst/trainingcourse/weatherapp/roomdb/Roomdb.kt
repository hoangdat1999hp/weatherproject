package gst.trainingcourse.weatherapp.roomdb

import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import android.content.Context

@Database(entities = [Cityroom::class],version = 1)
abstract class Roomdb:RoomDatabase() {
    abstract fun cityroomDAO():IDaoroom?
    companion object{
        private var INSTANCE:Roomdb?=null
        fun getAppdatabase(context: Context):Roomdb?{
            if (INSTANCE==null){
                INSTANCE = Room.databaseBuilder<Roomdb>(
                    context.applicationContext,Roomdb::class.java,"Appdb"
                )
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE
        }

    }
}