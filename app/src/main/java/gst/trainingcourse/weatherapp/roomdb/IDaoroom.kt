package gst.trainingcourse.weatherapp.roomdb

import androidx.room.*





@Dao
interface IDaoroom {
    @Query("Select * from cityrooms ORDER by id DESC")
    open fun getalldb(): MutableList<Cityroom>?

    @Query("SELECT * FROM cityrooms WHERE name LIKE :cityname ORDER by id DESC")
    fun gethistoryByName(cityname: String?): MutableList<Cityroom>?
    @Insert
    fun addhistory(city:Cityroom?)
    @Delete
    fun delete(city: Cityroom?)
}