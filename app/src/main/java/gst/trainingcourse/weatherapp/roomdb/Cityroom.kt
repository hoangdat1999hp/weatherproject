package gst.trainingcourse.weatherapp.roomdb

import androidx.annotation.NonNull
import androidx.room.*


@Entity(tableName = "cityrooms")
data class Cityroom(
    @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id") val id : Int = 0 ,
    @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "icon") val icon: String,
    @ColumnInfo(name = "date") val date: String?,
    @ColumnInfo(name = "time") val time: String?,
    @ColumnInfo(name = "degree") val nhietdo: String?,
)
