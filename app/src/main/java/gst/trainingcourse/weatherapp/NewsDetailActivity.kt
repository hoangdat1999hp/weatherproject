package gst.trainingcourse.weatherapp

import android.os.AsyncTask
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_news_detail.*
import kotlinx.android.synthetic.main.news_item.*
import org.jsoup.Jsoup
import org.jsoup.select.Elements

class NewsDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_news_detail)

        var title = intent.getStringExtra("TITLE")
        var link = intent.getStringExtra("LINK")
        var img = intent.getStringExtra("IMG")
        detail_title_tv.text = title
        Glide.with(this)
            .load(img)
            .apply(
                RequestOptions()
                .transform(CenterCrop())
                .transform(RoundedCorners(25)))
            .into(detail_img)
        ReadDetail().execute(link)
    }

    inner class ReadDetail(): AsyncTask<String, Void, String>(){
        override fun doInBackground(vararg params: String?): String {
            var content: StringBuilder = StringBuilder()
            var document: org.jsoup.nodes.Document = Jsoup.connect(params[0]).get()
            var deatail: Elements = document.select("p.Normal")
            for(i in deatail){
                content.append(i.text() + "\n")
            }
            return content.toString()
        }

        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)
            detail_content_tv.text = result
        }
    }
}