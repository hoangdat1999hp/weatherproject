package gst.trainingcourse.weatherapp

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitClient {
    fun getRetrofit(baseurl:String):Retrofit{
        var retrofit = Retrofit.Builder()
        retrofit.baseUrl(baseurl)
        retrofit.addConverterFactory(GsonConverterFactory.create())
        retrofit.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        return retrofit.build()
    }
    const val BASE_URL = "http://api.openweathermap.org/data/2.5/"
    const val BASE_URL2 = "http://api.openweathermap.org/data/2.5/forecast/"
    const val URL_THUMBNAIL = "http://openweathermap.org/img/w/"

    fun getAPI(): IDocAPI {
        return getRetrofit(BASE_URL).create(IDocAPI::class.java)
    }
    fun getAPI2(): IDocAPI {
        return getRetrofit(BASE_URL2).create(IDocAPI::class.java)
    }
}