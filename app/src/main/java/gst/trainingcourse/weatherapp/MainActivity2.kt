package gst.trainingcourse.weatherapp

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Window
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import gst.trainingcourse.weatherapp.fragment.ChartFragment
import gst.trainingcourse.weatherapp.fragment.HistoryFragment
import gst.trainingcourse.weatherapp.fragment.HomeFragment
import gst.trainingcourse.weatherapp.fragment.NewsFragment
import kotlinx.android.synthetic.main.item_history.*
import java.util.*

@RequiresApi(Build.VERSION_CODES.O)
class MainActivity2 : AppCompatActivity() {
    private var city = "a"
    lateinit var cal : Calendar
    override fun onCreate(savedInstanceState: Bundle?) {
        Log.d("TAG", city)
        cal = Calendar.getInstance()
        cal[Calendar.HOUR_OF_DAY] = 0
        cal[Calendar.MINUTE] = 0
        cal[Calendar.SECOND] = 0
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.activity_main2)
        LocalBroadcastManager.getInstance(this).registerReceiver(dataReciever, IntentFilter("getCity"))
        LocalBroadcastManager.getInstance(this).registerReceiver(cityReceiver, IntentFilter("getCity"))
//     binding = ActivityMain2Binding.inflate(layoutInflater)
//     setContentView(binding.root)

        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottom_navigation)
//        bottomNavigationView.setBackgroundColor(Color.DKGRAY)
        supportFragmentManager.beginTransaction().replace(
            R.id.fragment_container,
            HomeFragment("")
        ).commit()
        bottomNavigationView.setOnNavigationItemSelectedListener(onNav)

//        val brc = NotificationReceiver()
//        val intentFilter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
//        registerReceiver(brc, intentFilter)
        Log.d("TAG", city)

    }

    private val onNav =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            var selected: Fragment? = null
            when (item.itemId) {
                R.id.nav_home -> selected = HomeFragment("")
                R.id.nav_history -> selected = HistoryFragment()
                R.id.nav_news -> selected = NewsFragment()
                R.id.nav_chart -> selected = ChartFragment(city)
            }
            if (selected != null) {
                supportFragmentManager.beginTransaction().replace(
                    R.id.fragment_container,
                    selected
                ).commit()
            }
//            if(selected == ChartFragment()){
//
//            }
            true
        }
    private var dataReciever : BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val bundle = intent?.extras
            if(bundle!=null){
                    city = intent.getStringExtra("city").toString()
                    Log.d("TAG", city)
                    val intent = Intent(context, MyService::class.java)
                    intent.putExtra("cityname", city)
                    val pintent = PendingIntent.getService(context, 0, intent, 0)
                    val alarm =
                        context!!.getSystemService(Context.ALARM_SERVICE) as AlarmManager?
                    alarm!!.setRepeating(
                        AlarmManager.RTC_WAKEUP,
                        cal.getTimeInMillis(),
                        (1000 * 60).toLong(),
                        pintent
                    )
//                      startService(intent)
                    unregister()
            }
        }
    }
    private var cityReceiver : BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent?) {
            val bundle = intent?.extras
            if (bundle != null) {
                city = intent.getStringExtra("city").toString()
                Log.d("TAG", city + " 1")
            }
        }
    }
    private fun unregister(){
        LocalBroadcastManager.getInstance(this).unregisterReceiver(dataReciever)
    }

    override fun onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(dataReciever)
        LocalBroadcastManager.getInstance(this).unregisterReceiver(cityReceiver)
        super.onDestroy()
    }
}