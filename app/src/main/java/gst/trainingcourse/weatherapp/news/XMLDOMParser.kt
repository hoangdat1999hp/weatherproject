package gst.trainingcourse.weatherapp.news

import android.util.Log
import org.w3c.dom.Document
import org.w3c.dom.Element
import org.w3c.dom.Node
import org.xml.sax.InputSource
import org.xml.sax.SAXException
import java.io.IOException
import java.io.StringReader
import javax.xml.parsers.DocumentBuilder
import javax.xml.parsers.DocumentBuilderFactory
import javax.xml.parsers.ParserConfigurationException


class XMLDOMParser {
    fun getDocument(xml: String): Document {
        var db: DocumentBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder()
        var inputSource = InputSource()
        inputSource.setCharacterStream(StringReader(xml))
        inputSource.encoding = "UTF-8"
        var document: Document = db.parse(inputSource)
        return document
    }

    fun getValue(item: Element, name: String?): String {
        var nodes = item.getElementsByTagName(name)
        return getTextNodeValue(nodes.item(0))
    }

    private fun getTextNodeValue(elem: Node?): String {
        var child: Node?
        if (elem != null) {
            if (elem.hasChildNodes()) {
                child = elem.firstChild
                while (child != null) {
                    if (child.nodeType == Node.TEXT_NODE) {
                        return child.nodeValue
                    }
                    child = child.nextSibling
                }
            }
        }
        return ""
    }
}
