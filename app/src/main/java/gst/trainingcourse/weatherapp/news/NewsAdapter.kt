package gst.trainingcourse.weatherapp.news

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import gst.trainingcourse.weatherapp.R
import gst.trainingcourse.weatherapp.models_news.News
import kotlinx.android.synthetic.main.news_item.view.*

class NewsAdapter(var listNews: MutableList<News>, var context: Context, var listener: OnItemClickListener): RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {
    inner class NewsViewHolder(view: View) : RecyclerView.ViewHolder(view), View.OnClickListener {
        var imgSrc = view.news_thumb_img
        var title = view.news_title_tv

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            var position: Int = adapterPosition
            listener.OnItemClick(position)
        }
    }

    interface OnItemClickListener{
        fun OnItemClick(position: Int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
        return NewsViewHolder(view)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        Glide.with(context)
            .load(listNews[position].getImgSrc())
            .apply(RequestOptions()
                .transform(CenterCrop())
                .transform(RoundedCorners(25)))
            .into(holder.imgSrc)

        holder.title.text = listNews[position].getTitle()
    }

    override fun getItemCount(): Int {
        return listNews.size
    }
}